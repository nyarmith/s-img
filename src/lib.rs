use std::fs::File;
use std::io::Write;
use std::error::Error;

mod chunks;

fn write_to_ppm(name : &str, png : chunks::Png){
    let mut file = File::create(name).unwrap();
    let cbs = png.channel_bit_size();
    let bits_per_pixel = png.num_channels() * cbs;
    let header = format!("P6\n{} {}\n{}\n", png.data[0].len()*8/bits_per_pixel, png.data.len(), 2u32.pow(cbs as u32) - 1);
    file.write_all(header.as_bytes()).unwrap();
    for row in 0..png.data.len() {
        file.write_all(&png.data[row]).unwrap();
    }
}

pub mod png {
    use super::*;
    pub fn read(path: &str) -> Result<chunks::Png, Box<dyn Error>> {
        chunks::read_png(path)
    }

    pub fn write_to_ppm(path: &str, png : chunks::Png) {
        super::write_to_ppm(path, png);
    }
}

pub use self::png::*;


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn read_ms_paint_png() {
        // uncompressed test
        let png = png::read("mspaint_128x64_colors.png").unwrap();
        println!("{:?}", png.meta);
        assert_eq!(png.data.len(), 64);
        for row in &png.data {
            assert_eq!(row.len(), 128*3);
        }
        
        write_to_ppm("mspaint_128x64_colors.ppm", png);
    }

    #[test]
    fn read_banana_demo_png() {
        // compressed test
        let png = png::read("banana_demo_400x268_colors.png").unwrap();
        println!("{:?}", png.meta);
        assert_eq!(png.data.len(), 268);
        for row in &png.data {
            assert_eq!(row.len(), 400*3);
        }
        
        write_to_ppm("banana_demo_400x268_colors.ppm", png);
    }

    #[test]
    fn read_mono8_png() {
        // mono8 test
        let png = png::read("parrot_150x200_mono8.png").unwrap();
        println!("{:?}", png.meta);
        assert_eq!(png.data.len(), 200);
        for row in &png.data {
            assert_eq!(row.len(), 150);
        }
        
        write_to_ppm("parrot_150x200_mono8.ppm", png);
    }

    #[test]
    fn read_indexed_png() {
        let png = png::read("parrot_150x200_indexed.png").unwrap();
        println!("{:?}", png.meta);
        assert_eq!(png.data.len(), 200);
        for row in &png.data {
            assert_eq!(row.len(), 150);
        }
        
        write_to_ppm("parrot_150x200_indexed.ppm", png);
    }
}
