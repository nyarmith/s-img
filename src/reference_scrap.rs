const LIT_TABLE: [u16; 288] = [0;288];
const DIST_TABLE: [u16; 32] = [0;32];
const LENGTH_EXTRA: [(u8, u8); 29] = [(0,0);29];
const DIST_EXTRA: [u8; 30] = [0;30];

fn decode_block(data: &[u8], table_lit: &[u16], table_dist: &[u16]) -> Vec<u8> {
    let mut output = Vec::new();
    let mut pos = 0;
    loop {
        let (lit, len) = decode_huffman(&data[pos..], table_lit).unwrap();
        pos += len;
        if lit < 256 {
            output.push(lit as u8);
        } else if lit == 256 {
            break;
        } else {
            let (len_bits, len_extra) = LENGTH_EXTRA[(lit - 257) as usize];
            let (dist, len) = decode_huffman(&data[pos..], table_dist).unwrap();
            pos += len;
            let (dist_bits, dist_extra) = DIST_EXTRA[(dist as usize)];
            let mut offset = dist_extra as usize;
            offset |= (data[pos] as usize) << dist_bits;
            pos += 1;
            let mut length = len_extra as usize;
            length |= (data[pos] as usize) << len_bits;
            pos += 1;
            output.extend_from_slice(&output[output.len() - offset..][..length]);
        }
    }
    output
}

fn decode(data: &[u8]) -> Vec<u8> {
    let mut output = Vec::new();
    let mut pos = 0;
    loop {
        let bfinal = (data[pos] & 0x01) != 0;
        let btype = (data[pos] & 0x03) >> 1;
        pos += 1;
        match btype {
            0 => {
                let len = ((data[pos + 1] as usize) << 8) | (data[pos] as usize);
                pos += 2;
                let nlen = ((data[pos + 1] as usize) << 8) | (data[pos] as usize);
                pos += 2;
                output.extend_from_slice(&data[pos..][..len]);
                pos += len;
            }
            1 => {
                let table_lit = &LIT_TABLE;
                let table_dist = &DIST_TABLE;
                let block = decode_block(&data[pos..], table_lit, table_dist);
                output.extend_from_slice(&block);
                pos += block.len() + 1;
            }
            _ => panic!("Invalid block type"),
        }
        if bfinal {
            break;
        }
    }
    output
}


#[test]
fn test_ai_code() {
    let compressed_data = &[0x78, 0x9c, 0x63, 0x60, 0x60, 0x60, 0x00, 0x00, 0x00, 0xff, 0xff];
    let decompressed_data = decode(compressed_data);
    let expected_data = &[0x61, 0x61, 0x61, 0x61, 0x61, 0x61, 0x61, 0x61, 0x61, 0x61, 0x61, 0x61, 0x61, 0x61, 0x61, 0x61, 0x61, 0x61, 0x61, 0x61, 0x61, 0x61, 0x61, 0x61, 0x61, 0x61, 0x61, 0x61, 0x61, 0x61, 0x61, 0xff, 0xff];
    assert_eq!(decompressed_data, expected_data);
}
