use std::collections::BinaryHeap;
use std::collections::HashMap;
use std::hash;
use std::cmp;

// helper types
struct BitStreamParser<'a> {
    data: &'a[u8],
    pos: usize,
    len: usize
}

impl BitStreamParser<'_> {
    fn new(data : &[u8]) -> Self{
        BitStreamParser {
            data,
            pos: 0,
            len: data.len()*8
        }
    }

    fn is_code(&self, key: usize) -> bool {
        // create mask for key
        //let mask: usize = !((-((key & (key-1))^key))>>1);

        // TODO: get bits for our mask
        // reference; https://github.com/nayuki/Simple-DEFLATE-decompressor/blob/master/cpp/DeflateDecompress.hpp
        true
    }
}

pub fn lz77_decode(data: &[u8], window_size: usize) {
}

pub fn lz77_encode(data: &[u8], window_size: usize) {
}


pub fn huffman_decode(data: &[u8], prefix_codes: &[u8]) {
}

pub fn huffman_encode<T: hash::Hash>(data: &[T]) -> (HashMap<String,T>,Vec<T>) where T: Eq {
    // step 1 - frequency analysis of T
    let mut freq_map = HashMap::<T,usize>::new();
    for val in data {
        if let Some(n) = freq_map.get_mut(val) {
            *n += 1;
        } else {
            freq_map.insert(*val, 0);
        }
    }

    // step 2 - put frequencies in heap
    let mut heap = BinaryHeap::<Box<HNode<T>>>::new();
    for (k,v) in freq_map {
        heap.push(Box::new(HNode::new(Some(k), v, None, None)));
    }

    // step 3 - build tree
    while heap.len() != 1 {
        let mut A = heap.pop().unwrap();
        let mut B = heap.pop().unwrap();

        let branch = HNode::new(None, A.freq + B.freq, Some(A), Some(B));
    }

    // step 4 - Create the codes
    let mut codes : HashMap<T, String> = HashMap::new();

    // TODO: Traverse tree -- lol

    // step 5 - Compress
    let compressed = Vec::<T>::new();
    // finally, provide the map in a way that's convenient to decode
    //
    let inverted_map: HashMap<String, T> = codes.into_iter().map(|(k,v)| (v,k)).collect();

    (inverted_map, compressed)
}

struct HNode<T> {
    symbol : Option<T>,
    freq : usize,
    left : Option<Box<HNode<T>>>,
    right: Option<Box<HNode<T>>>
}

impl<T> HNode<T> {
    fn new(symbol: Option<T>, freq: usize, left: Option<Box<Self>>, right: Option<Box<Self>>) -> Self{
        HNode::<T> {
            symbol,
            freq,
            left,
            right
        }
    }
}

impl<T> cmp::Ord for HNode<T> {
    fn cmp(&self, other: &Self) -> cmp::Ordering {
        other.freq.cmp(&self.freq)
    }
}

impl<T> cmp::PartialOrd for HNode<T> {
    fn partial_cmp(&self, other: &Self) -> Option<cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl<T> cmp::PartialEq for HNode<T> {
    fn eq(&self, other: &Self) -> bool {
        self.freq == other.freq
    }
}

impl<T> cmp::Eq for HNode<T> { }

#[test]
fn ordering_check() {
    let mut heap = BinaryHeap::<HNode<char>>::new();
    let c1 = HNode::new(Some('a'), 10, None, None);
    let c2 = HNode::new(Some('b'), 2, None, None);
    let c3 = HNode::new(Some('d'), 4, None, None);
    let c4 = HNode::new(Some('r'), 1, None, None);
    
    heap.push(c1);
    heap.push(c2);
    heap.push(c3);
    heap.push(c4);
    
    assert_eq!(heap.pop().unwrap().symbol.unwrap(), 'r');
    assert_eq!(heap.pop().unwrap().symbol.unwrap(), 'b');
    assert_eq!(heap.pop().unwrap().symbol.unwrap(), 'd');
    assert_eq!(heap.pop().unwrap().symbol.unwrap(), 'a'); 
}

#[test]
fn check_huff_encode() {
    let data = b"abcdcbaacaea";
    let (codes, encoded) = huffman_encode(data);

    // idk how to consume stuff in rust lol
}

fn decode_huffman(data: &[u8], table: &[u16]) -> Option<(u16, usize)> {
    let mut code = 0;
    let mut len = 0;
    for i in 0..16 {
        code |= (data[0] as u16) << i;
        len += 1;
        if code < table[i as usize] {
            return Some((table[i as usize], len));
        }
    }
    None
}
