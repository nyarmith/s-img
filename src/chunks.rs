#![allow(non_camel_case_types)]

use itertools::izip;
use flate2::write::ZlibEncoder;
use flate2::read::ZlibDecoder;


use std::fs;
use std::error::Error;
use std::any::type_name;
use std::fmt;
use std::io::Read;

#[derive(Debug)]
pub struct Png {
    pub meta: PngMetaData,
    pub data: Vec<Vec<u8>>
}


pub fn read_png(path: &str) -> Result<Png, Box<dyn Error>> {
    let data = fs::read(path)?;
    if !is_valid_png_header(&data) {
        return Err(Box::new(PngError(format!("Invalid image header in {}", path))));
    }
    let mut data = &data[8..];

    let mut partial_png = PartialPng::new();

    let readers : Vec<Box<dyn PngChunkProcessor>> = vec![Box::new(IHDR), Box::new(sRGB), Box::new(gAMA), Box::new(pHYs), Box::new(tEXt), Box::new(IDAT), Box::new(IEND)];

    // how do we tell we're at the end? how do we deal with an abrupt ending vs a declared ending?
    while !partial_png.done {
        let label = &data[4..8];
        let chunk_len = u32::from_be_bytes(data[..4].try_into().unwrap()) as usize;
        match readers.iter().find(|r| r.label() == label){
            Some(reader) => {
                println!("{} chunk found", String::from_utf8_lossy(reader.label()));
                let chunk_slice = &data[8..];
                reader.process(&chunk_slice[..chunk_len], &mut partial_png);
                //TODO: impl CRC -- it's right there in the spec!
                //let chunk_crc = chunk_slice[chunk_len..(chunk_len+4)];
            },
            None => {
                let (is_critical, is_public, _, _) = chunk_properties(label);
                let label = String::from_utf8_lossy(label);
                if is_critical {
                    eprintln!("No such processor found for critical chunk type: '{}'! Breaking and returning chunks so far", label);
                    break
                } else {
                    eprintln!("No such processor for non-critical {} chunk type: '{}', continuing...", if is_public {"public"} else {"private"}, label);
                }
            }
        }
        data = &data[(chunk_len+12)..];
    }

    let png = Png::from(partial_png);
    Ok(png)
}

// PNG chunks as structs
struct IHDR;
struct sRGB;
struct gAMA;
struct pHYs;
struct tEXt;
struct IDAT;
struct IEND;
struct eXIf;

// trait for our chunk structs


trait PngChunkProcessor {
    fn label(&self) -> &[u8] {
        type_name::<Self>().split("::").last().unwrap().as_bytes()
    }
    fn process(&self, data: &[u8], png: &mut PartialPng);
}

struct PartialPng {
    meta: PngMetaData,
    data: Vec<u8>,
    done: bool
}

#[derive(Debug)]
pub struct PngMetaData {
    width: u32,
    height: u32,
    bit_depth: u8,
    color_type: ColorType,
    compression: Compression,
    filter: Filter,
    interlace: Interlace,
    gamma: f32,
    ppu: (u32,u32),
    meter: bool,
    text: String,
    srgb_intent: Option<sRGB_intent>
}

impl PartialPng {
    fn new() -> Self {
        PartialPng{
            meta: PngMetaData::new(),
            data: Vec::new(),
            done: false
        }
    }
}

impl PngMetaData {
    fn new() -> Self {
        PngMetaData {
            width: 0,
            height: 0,
            bit_depth: 0,
            color_type: ColorType::Greyscale,
            compression: Compression::Deflate,
            filter: Filter::Adaptive,
            interlace: Interlace::None,
            gamma: 0.0,
            ppu: (0,0),
            meter: false,
            text: "".to_string(),
            srgb_intent: None
        }
    }
}

#[derive(Debug)]
enum Compression {
    Deflate
}

#[derive(Debug)]
enum Filter {
    Adaptive
}

#[derive(Debug)]
enum Interlace {
    None,
    Adam7
}

#[derive(Debug)]
enum ColorType {
    Greyscale,
    Truecolor,
    IndexedColor,
    GreyscaleAlpha,
    TruecolorAlpha
}

impl ColorType {
    fn num_channels(&self) -> usize {
        match self {
            ColorType::Greyscale => 1,
            ColorType::IndexedColor => 1,
            ColorType::GreyscaleAlpha => 2,
            ColorType::Truecolor => 3,
            ColorType::TruecolorAlpha => 4
        }
    }
}

#[derive(Debug)]
enum sRGB_intent {
    Perceptual,
    Relative_Colorimetric,
    Saturation,
    Absolute_Colorimetric
}

struct PngError(String);

impl fmt::Display for PngError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "PNG Error - {}", self.0)
    }
}

impl Error for PngError {}

impl fmt::Debug for PngError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fmt::Display::fmt(self, f)
    }
}


impl Png {
    fn new() -> Self {
        Self {
            meta: PngMetaData::new(),
            data: Vec::new()
        }
    }

    pub fn num_channels(&self) -> usize {
        self.meta.color_type.num_channels()
    }

    pub fn channel_bit_size(&self) -> usize {
        self.meta.bit_depth as usize
    }

    fn from(partial : PartialPng) -> Self {
        // 1. decompress data, probably somehow more clever than we do here
        let mut decoder = ZlibDecoder::new(&partial.data[..]);
        let data : Vec<u8> = decoder.bytes().map(|b| b.unwrap()).collect();

        // 2. undo any filters applied
        // 2a get pixels per line
        let bits_per_pixel = partial.meta.color_type.num_channels() * (partial.meta.bit_depth as usize);
        let line_size = (partial.meta.width as usize * bits_per_pixel) / 8 + 1;

        // 2b over each line, unapply filter
        let bpp = usize::max(bits_per_pixel/8, 1);
        let mut data : Vec<Vec<u8>> = data.chunks(line_size).map(|chunk| Vec::from(chunk)).collect();
        for y in 0..data.len() {
            let line = &data[y];
            let filter = line[0];
            // modifying a copy of the line, but that copy is being modded in-place (TODO figure
            // out mutable borrow of og line that lets it be in-place, and resize or ignore first pixel away)
            let mut line = line[1..].to_vec();
            match filter {
                // no method, just cut off filter byte
                0 => {
                },
                // subtract left pixel filtering
                1 => {
                    // todo: replace with something elegant like folding over each channel
                    for x in bpp..line.len() {
                        line[x] = line[x].wrapping_add(line[x-bpp]);
                    }
                },
                // subtract the above pixel
                2 =>  {
                    let above = &data[y-1];
                    for x in 0..line.len() {
                        line[x] = line[x].wrapping_add(above[x]);
                    }
                },
                // averaging (left and above)
                3 =>  {
                    let above = &data[y-1];
                    for x in 0..bpp {
                        line[x] = line[x].wrapping_add(above[x]/2);
                    }
                    for x in bpp..line.len() {
                        line[x] = line[x].wrapping_add( ((line[x-bpp] as u16 + above[x] as u16)/2) as u8)
                    }
                }

                // Paeth filtering
                4 => {
                    let above = &data[y-1];
                    for x in 0..bpp {
                        line[x] = line[x].wrapping_add(paeth(0, above[x], 0));
                    }
                    for x in bpp..line.len() {
                        line[x] = line[x].wrapping_add(paeth(line[x-bpp], above[x], above[x-bpp]));
                    }
                }
                _ => {
                    panic!("Error -- unknown line filter type: {}", line[0]);
                }
            }
            data[y] = line;
        }

        Self {
            meta: partial.meta,
            data
        }
    }
}

fn is_valid_png_header(data: &Vec<u8>) -> bool {
    let header = [0x89, b'P', b'N', b'G', 0x0D, 0x0A, 0x1A, 0x0A];
    header == data[0..8]
}

fn chunk_properties(label : &[u8]) -> (bool, bool, bool, bool) {
    let fifth_bit = 0b10000 as u8;
    let is_critical = (fifth_bit & label[0]) != 0;
    let is_private = (fifth_bit & label[0]) != 0;
    let is_reserved = (fifth_bit & label[0]) == fifth_bit;
    let is_safe_to_copy = (fifth_bit & label[0]) != 0;
    (is_critical, is_private, is_reserved, is_safe_to_copy)
}

impl PngChunkProcessor for IHDR {
    fn process(&self, data: &[u8], png: &mut PartialPng) {
        png.meta.width = u32::from_be_bytes(data[..4].try_into().unwrap());
        png.meta.height = u32::from_be_bytes(data[4..8].try_into().unwrap());
        png.meta.bit_depth = data[8];
        png.meta.color_type = match data[9] {
            0 => ColorType::Greyscale,
            2 => ColorType::Truecolor,
            3 => ColorType::IndexedColor,
            4 => ColorType::GreyscaleAlpha,
            6 => ColorType::TruecolorAlpha,
            _ => panic!("Invalid color type {} in IHDR!", data[8])
        };

        // check validity of bit_depth with color type
        match (&png.meta.color_type, png.meta.bit_depth) {
            (ColorType::Greyscale,      n) =>  assert!( [1,2,4,8,16].contains(&n), "Invalid depth for {:?}, '{}'", png.meta.color_type, png.meta.bit_depth),
            (ColorType::Truecolor,      n) =>  assert!(       [8,16].contains(&n), "Invalid depth for {:?}, '{}'", png.meta.color_type, png.meta.bit_depth),
            (ColorType::IndexedColor,   n) =>  assert!( [1,2,4,8]   .contains(&n), "Invalid depth for {:?}, '{}'", png.meta.color_type, png.meta.bit_depth),
            (ColorType::GreyscaleAlpha, n) =>  assert!(       [8,16].contains(&n), "Invalid depth for {:?}, '{}'", png.meta.color_type, png.meta.bit_depth),
            (ColorType::TruecolorAlpha, n) =>  assert!(       [8,16].contains(&n), "Invalid depth for {:?}, '{}'", png.meta.color_type, png.meta.bit_depth)
        }

        png.meta.compression = match data[10] {
            0 => Compression::Deflate,
            _ => panic!("Inavlid compression method {} in IHDR", data[10])
        };

        png.meta.filter = match data[11] {
            0 => Filter::Adaptive,
            _ => panic!("Inavlid filter method {} in IHDR", data[11])
        };

        png.meta.interlace = match data[12] {
            0 => Interlace::None,
            1 => Interlace::Adam7,
            _ => panic!("Inavlid interlace method {} in IHDR", data[11])
        };
    }
}

impl PngChunkProcessor for sRGB {
    fn process(&self, data: &[u8], png: &mut PartialPng) {
        let intent = data[0];
        let intent = match intent {
            0x0 => sRGB_intent::Perceptual,
            0x1 => sRGB_intent::Relative_Colorimetric,
            0x2 => sRGB_intent::Saturation,
            0x3 => sRGB_intent::Absolute_Colorimetric,
            _ => panic!("Unknown srgb chunk intent")
        };
        png.meta.srgb_intent = Some(intent);
    }
}

impl PngChunkProcessor for gAMA {
    fn process(&self, data: &[u8], png: &mut PartialPng) {
        let raw_gamma = u32::from_be_bytes(data[..4].try_into().unwrap());
        png.meta.gamma = ((raw_gamma as f64) / 1000.0) as f32;
    }
}

impl PngChunkProcessor for pHYs {
    fn process(&self, data: &[u8], png: &mut PartialPng) {
        // pixels per unit on each dimension
        let ppu_x = u32::from_be_bytes(data[..4].try_into().unwrap());
        let ppu_y = u32::from_be_bytes(data[4..8].try_into().unwrap());
        png.meta.ppu = (ppu_x, ppu_y);
        let unit = data[8];
        png.meta.meter = match unit {
            0 => false, // unit type is unknown
            1 => true,  // unit type is meter
            _ => false, // unit type is very unknown!
        }
    }
}

impl PngChunkProcessor for eXIf {
    fn process(&self, _data: &[u8], _png: &mut PartialPng) {
        println!("In an eXIf chunk, not implementing the massive unit yet");
    }
}

impl PngChunkProcessor for tEXt {
    fn process(&self, data: &[u8], png: &mut PartialPng) {
        let nul_ind = data.iter().position(|&c| c == b'\0').expect("Error finding null terminator in tEXt chunk!");
        let o = nul_ind + 1;
        png.meta.text = format!("{}:{}", String::from_utf8_lossy(&data[..nul_ind]), String::from_utf8_lossy(&data[o..]));
    }
}

impl PngChunkProcessor for IDAT {
    fn process(&self, data: &[u8], png: &mut PartialPng) {
        png.data.extend_from_slice(data);
    }
}

impl PngChunkProcessor for IEND {
    fn process(&self, _data: &[u8], png: &mut PartialPng) {
        png.done = true;
    }
}

fn paeth(left: u8, up: u8, upleft: u8) -> u8{
    let p = (left as i16 + up as i16 - upleft as i16);
    // maybe this is the issue?
    let p_left = (p - left as i16).abs();
    let p_up = (p - up as i16).abs();
    let p_upleft = (p - upleft as i16).abs();
    
    let paeth;
    if p_left <= p_up && p_left <= p_upleft {
        paeth = left;
    } else if p_up <= p_upleft {
        paeth = up;
    } else {
        paeth = upleft;
    }
    paeth
}

#[test]
fn label_test() {
    let idat = IDAT;
    let exif = eXIf;
    let mut buf = String::new();

    assert_eq!(idat.label(), b"IDAT");
    assert_eq!(exif.label(), b"eXIf");
}
